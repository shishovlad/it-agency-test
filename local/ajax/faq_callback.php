<?
require ($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');
?>

<?
// Валидация
$is_correct_phone = preg_match('/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/', $_POST['phone']);
if (!$is_correct_phone) {
    echo 0;
    return;
}

// Запись в инфоблок
$arLoadFormArray = Array("MODIFIED_BY" => $USER->GetID(),
    "IBLOCK_SECTION_ID" => false,
    "IBLOCK_ID" => 1,
    "NAME" => $_POST['phone'],
    "ACTIVE" => "N",
);
CModule::IncludeModule("iblock"); 
$element = new CIBlockElement; 
if($form_ID = $element->Add($arLoadFormArray)){ 
    // mail();
    echo 1;
} else {
    echo -1;
}
?>

<?
require ($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/epilog_after.php');
?>