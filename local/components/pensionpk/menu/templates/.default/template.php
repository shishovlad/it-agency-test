<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>

<ul class="nav__ul">

<?
foreach($arResult as $arItem):
?>
	<?if($arItem["SELECTED"]):?>
		<li class="nav__li active">
    <?else:?>
        <li class="nav__li">
    <?endif?>
            <a href="<?=$arItem[1]?>" class="nav__link"><?=$arItem[0]?></a>
        </li>
	
<?endforeach?>

</ul>
<?endif?>