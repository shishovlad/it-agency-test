$(document).ready(function() {
    ymaps.ready(yandexMap);

    function yandexMap() { 
        var myMap = new ymaps.Map("map", {
            center: [54.322529, 48.405308],
            zoom: 17,
            controls: []
        }); 
        
        var myPlacemark = new ymaps.Placemark([54.322631, 48.401093], {
            hintContent: '',
            balloonContent: ''
        }, {
            iconLayout: 'default#image',
            iconImageHref: '/local/templates/pensionpk/img/map-geo.png',
            iconImageSize: [50, 72],
            iconImageOffset: [-5, -38]
        });
        
        myMap.geoObjects.add(myPlacemark);
        myMap.behaviors
        .disable(['drag', 'rightMouseButtonMagnifier'])
        .disable('scrollZoom');
    }


    $(".js-callback-btn").on("click", function() {
        if (!$('.js-callback_checkbox').prop("checked")) {
            alert('Поставьте флажок согласия на обработку перс. данных!');
            return false;
        }

        var tel = $('.js-callback_phone').val();
        if (tel == "") {
            alert('Форма не заполнена');
            return false;
        }

        $('.js-callback_phone').prop("disabled", true);

        $.ajax({
          method: "POST",
          url: "/local/ajax/faq_callback.php",
          data: {
            phone: tel
          },
          success: function(data) {
            switch (+data) {
              case -1:
                alert('Ошибка записи!');
                break;
              case 1:
                $('#successForm h3').html(tel);
                $('#successForm').modal('show');
                $('.js-callback_phone').val("");
                break;
              case 0:
                alert('Форма заполнена некорректно');
                break;
            }
          }
        });

        $('.js-callback_phone').prop("disabled", false);
    });
});
