<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
IncludeTemplateLangFile(__FILE__);
?>
<html prefix="og: http://ogp.me/ns#">
<head>
    <title><?$APPLICATION->ShowTitle()?></title>
    <?$APPLICATION->ShowHead();?>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, minimum-scale=1, user-scalable=no">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">

    <!-- Favicon -->
    <link rel="shortcut icon" href="favicon.ico" type="image/x-icon" />
    <link rel="apple-touch-icon" href="favicon.ico" />

    <!-- CSS -->
    <link rel='stylesheet prefetch' href="<?= SITE_TEMPLATE_PATH; ?>/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="<?= SITE_TEMPLATE_PATH; ?>/css/style.css">

    <!-- Fonts -->
    <link rel="stylesheet" href="<?= SITE_TEMPLATE_PATH; ?>/css/open-sans.css">

    <!-- JS -->
    <script src="<?= SITE_TEMPLATE_PATH; ?>/js/jquery.3.1.0.min.js" type="text/javascript"></script>
    <script src="<?= SITE_TEMPLATE_PATH; ?>/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="https://api-maps.yandex.ru/2.1/?lang=ru_RU" type="text/javascript"></script>
    <script src="<?= SITE_TEMPLATE_PATH; ?>/js/main.js" type="text/javascript"></script>
</head>
<body>
<?$APPLICATION->ShowPanel()?>