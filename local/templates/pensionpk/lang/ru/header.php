<?
$MESS['header']['btn-callback'] = "Обратный звонок";

$MESS['section-calc']['item'] = [
    [
    "label" => "Сумма сбережений",
    "value" => "200 000"
    ], [
        "label" => "На срок/ мес.",
        "value" => [3, 6, 12]
    ], [
        "label" => "Ежемесячное пополнение",
        "value" => "2 000"
    ], [
        "label" => "Выплата процентов",
        "value" => ["В конце срока", "Ежемесячно"]
    ]
];

$MESS['section-calc']['checkbox_bottom'] = "<b>+1%</b> к ставке для пенсионеров";


$MESS['section-calc-res']['item'] = [
    ["head" => "Вы получите"],
    ["head" => "Доход"],
    ["head" => "Ставка"],
    ["head" => "Сбережения застрахованы"]
];

$MESS['section-calc-res']['links'] = [
    "Полные условия", "График начислений", "Сохранить результат", "Посмотреть адрес на карте"
];

$MESS['section-faq-callback']['placeholder-phone'] = "Введите ваш номер";
$MESS['section-faq-callback']['text-data-check'] = "Согласен на обработку перс. данных";
$MESS['section-faq-callback']['text-btn'] = "Перезвоните мне";
$MESS['section-faq-callback']['text-under-btn'] = "Перезвоним за 15 минут";

$MESS['modals']['successForm'] = [
    'head' => "Спасибо!",
    'info' => "Специалист перезвонит вам по номеру",
    'btn' => "Хорошо"
];