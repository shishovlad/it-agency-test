<div class="modal fade bs-example-modal-sm" id="successForm" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="modal-body">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel"><?= $MESS['modals']['successForm']['head']; ?></h4>
        <p><?= $MESS['modals']['successForm']['info']; ?></p>
        <h3></h3>
        <button type="button" class="btn btn-green" data-dismiss="modal"><?= $MESS['modals']['successForm']['btn']; ?></button>
      </div>
    </div>
  </div>
</div>