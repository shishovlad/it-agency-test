<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');
?>

    <header>
        <div class="container-fluid">
            <div class="row">
                <div class="b-logo">
                    <!-- <a href="/"> -->
                        <img src="<?= SITE_TEMPLATE_PATH; ?>/img/logo.png" alt="">
                    <!-- </a> -->
                </div>
                <div class="nav__hamburger navbar-toggle collapsed" data-toggle="collapse" data-target="#menu"></div>
                <nav class="collapse" id="menu">
                    <?$APPLICATION->IncludeComponent("pensionpk:menu",".default", Array(
                            "ROOT_MENU_TYPE" => "top", 
                            "MAX_LEVEL" => "1", 
                            "CHILD_MENU_TYPE" => "top", 
                            "USE_EXT" => "Y",
                            "DELAY" => "N",
                            "ALLOW_MULTI_SELECT" => "Y",
                            "MENU_CACHE_TYPE" => "N", 
                            "MENU_CACHE_TIME" => "3600", 
                            "MENU_CACHE_USE_GROUPS" => "Y", 
                            "MENU_CACHE_GET_VARS" => "" 
                        )
                    );?>
                </nav>
                <div class="b-phone">
                    <? $APPLICATION->IncludeFile('/local/include/header/phone.php'); ?>
                    <a class="btn btn-green b-phone__btn" href="#faq-callback"><?= $MESS['header']['btn-callback']; ?></a>
                </div>
            </div>
        </div>
    </header>


    <section class="b-slider">
        <div class="container-fluid">
            <div class="row">
                <h1><? $APPLICATION->IncludeFile('/local/include/slider/h1.php'); ?></h1>
                <h3><? $APPLICATION->IncludeFile('/local/include/slider/h3.php'); ?></h3>
                <ul class="b-slider__ul">
                    <? $APPLICATION->IncludeFile('/local/include/slider/list.php'); ?>
                </ul>
            </div>
        </div>
    </section>


    <section class="b-calc" id="calc">
        <div class="container-fluid">
            <div class="row">
                <div class="calc">
                    <div class="calc__item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label for="calc-1">
                            <?= $MESS['section-calc']['item'][0]['label']; ?>
                        </label>
                        <div class="calc__input-block">
                            <input type="text" class="calc__input" id="calc-1" value="<?= $MESS['section-calc']['item'][0]['value']; ?>">
                        </div>
                    </div>
                    <div class="calc__item col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <label><?= $MESS['section-calc']['item'][1]['label']; ?></label>
                        <div class="radio-custom">
                            <label class="radio-custom__label calc__radio-label col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                <input class="radio-custom__old" type="radio" name="calc-radio-mes" value="<?= $MESS['section-calc']['item'][1]['value'][0]; ?>">
                                <span class="radio-custom__new"><?= $MESS['section-calc']['item'][1]['value'][0]; ?></span>
                            </label>
                            <label class="radio-custom__label calc__radio-label col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                <input class="radio-custom__old" type="radio" name="calc-radio-mes" value="<?= $MESS['section-calc']['item'][1]['value'][1]; ?>">
                                <span class="radio-custom__new"><?= $MESS['section-calc']['item'][1]['value'][1]; ?></span>
                            </label>
                            <label class="radio-custom__label calc__radio-label col-lg-4 col-md-4 col-sm-4 col-xs-3">
                                <input class="radio-custom__old" type="radio" name="calc-radio-mes" value="<?= $MESS['section-calc']['item'][1]['value'][2]; ?>" checked="checked">
                                <span class="radio-custom__new calc__radio"><?= $MESS['section-calc']['item'][1]['value'][2]; ?></span>
                            </label>
                        </div>
                    </div>
                    <div class="calc__item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <label for="calc-3"><?= $MESS['section-calc']['item'][2]['label']; ?></label>
                        <div class="calc__input-block">
                            <input type="text" class="calc__input" id="calc-3" value="<?= $MESS['section-calc']['item'][2]['value']; ?>">
                        </div>
                    </div>
                    <div class="calc__item col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <label><?= $MESS['section-calc']['item'][3]['label']; ?></label>
                        <div class="radio-custom radio-custom--small">
                            <label class="radio-custom__label calc__radio-label col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <input class="radio-custom__old" type="radio" name="calc-radio-per" value="<?= $MESS['section-calc']['item'][3]['value'][0]; ?>">
                                <span class="radio-custom__new"><?= $MESS['section-calc']['item'][3]['value'][0]; ?></span>
                            </label>
                            <label class="radio-custom__label calc__radio-label col-lg-6 col-md-6 col-sm-6 col-xs-6">
                                <input class="radio-custom__old" type="radio" name="calc-radio-per" value="<?= $MESS['section-calc']['item'][3]['value'][1]; ?>" checked="checked">
                                <span class="radio-custom__new"><?= $MESS['section-calc']['item'][3]['value'][1]; ?></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                        <label class="checkbox-custom calc__checkbox-label">
                            <input class="checkbox-custom__old" type="checkbox" name="calc-checkbox" checked="checked">
                            <span class="checkbox-custom__new"></span>
                            <span class="checkbox-custom__text"><?= $MESS['section-calc']['checkbox_bottom']; ?></span>
                        </label>
                    </div>
                    <div class="clearfix"></div>
                </div>

                <div class="calc-res">
                    <div class="calc-res__item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <h5><?= $MESS['section-calc-res']['item'][0]['head']; ?></h5>
                        <div class="calc-res__sum">234 000&nbsp;₽</div>
                    </div>
                    <div class="calc-res__item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <h5><?= $MESS['section-calc-res']['item'][1]['head']; ?></h5>
                        <div class="calc-res__sum">34 000&nbsp;₽</div>
                    </div>
                    <div class="calc-res__item col-lg-2 col-md-2 col-sm-6 col-xs-12">
                        <h5><?= $MESS['section-calc-res']['item'][2]['head']; ?></h5>
                        <div class="calc-res__per">17%</div>
                    </div>
                    <div class="calc-res__item col-lg-3 col-md-3 col-sm-6 col-xs-12">
                        <h5><?= $MESS['section-calc-res']['item'][3]['head']; ?></h5>
                        <img src="<?= SITE_TEMPLATE_PATH; ?>/img/mpos.png" alt="">
                    </div>

                    <div class="clearfix"></div>

                    <div class="calc-res__links">
                        <a href="#" class="dashed"><?= $MESS['section-calc-res']['links'][0]; ?></a>
                        <a href="#" class="dashed"><?= $MESS['section-calc-res']['links'][1]; ?></a>
                        <a href="#"><?= $MESS['section-calc-res']['links'][2]; ?></a>
                        <a href="#" class="dashed"><?= $MESS['section-calc-res']['links'][3]; ?></a>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section class="b-programs" id="programs">
        <div class="container-fluid">
                <h2><? $APPLICATION->IncludeFile('/local/include/programs/h2.php'); ?></h2>
                <h4><? $APPLICATION->IncludeFile('/local/include/programs/h4.php'); ?></h4>
            <div class="row row-30">
                <div class="program table">
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="program__item">
                            <div class="program__head"><? $APPLICATION->IncludeFile('/local/include/programs/head-1.php'); ?></div>
                            <div class="program__content">
                                <? $APPLICATION->IncludeFile('/local/include/programs/content-1.php'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="program__item">
                            <div class="program__head"><? $APPLICATION->IncludeFile('/local/include/programs/head-2.php'); ?></div>
                            <div class="program__content">
                                <? $APPLICATION->IncludeFile('/local/include/programs/content-2.php'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="program__item">
                            <div class="program__head"><? $APPLICATION->IncludeFile('/local/include/programs/head-3.php'); ?></div>
                            <div class="program__content">
                                <? $APPLICATION->IncludeFile('/local/include/programs/content-3.php'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <div class="program__item">
                            <div class="program__head"><? $APPLICATION->IncludeFile('/local/include/programs/head-3.php'); ?></div>
                            <div class="program__content">
                                <? $APPLICATION->IncludeFile('/local/include/programs/content-4.php'); ?>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>


    <section class="b-callback b-callback--grey" id="faq-callback">
        <div class="container-fluid">
            <div class="row">
                <h2><? $APPLICATION->IncludeFile('/local/include/faq-callback/header.php'); ?></h2>
                <div class="b-callback__form">
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12 b-callback__phone">
                        <? $APPLICATION->IncludeFile('/local/include/faq-callback/phone.php'); ?>
                        <p class="b-callback__schedule b-callback__text">
                            <? $APPLICATION->IncludeFile('/local/include/faq-callback/schedule.php'); ?>
                        </p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <input type="text" name="callback_phone" class="js-callback_phone" placeholder="<?= $MESS['section-faq-callback']['placeholder-phone']; ?>">
                        <div class="b-callback__data">
                            <label class="checkbox-custom  b-callback__checkbox-lable">
                                <input class="checkbox-custom__old js-callback_checkbox" type="checkbox" name="calc-checkbox" checked="checked">
                                <span class="checkbox-custom__new checkbox-custom__new--small"></span>
                                <span class="checkbox-custom__text b-callback__text b-callback__checkbox-link link dashed"><?= $MESS['section-faq-callback']['text-data-check']; ?></span>
                            </label>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="btn btn-yellow js-callback-btn"><?= $MESS['section-faq-callback']['text-btn']; ?></div>
                        <p class="b-callback__text"><?= $MESS['section-faq-callback']['text-under-btn']; ?></p>
                    </div>
                    <div class="clearfix"></div>
                </div>

                
                <div class="b-callback__steps">
                    <h3><? $APPLICATION->IncludeFile('/local/include/steps/head.php'); ?></h3>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="b-callback__steps-info">
                            <h4><? $APPLICATION->IncludeFile('/local/include/steps/info-head-1.php'); ?></h4>
                            <p><? $APPLICATION->IncludeFile('/local/include/steps/info-text-1.php'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="b-callback__steps-info">
                            <h4><? $APPLICATION->IncludeFile('/local/include/steps/info-head-2.php'); ?></h4>
                            <p><? $APPLICATION->IncludeFile('/local/include/steps/info-text-2.php'); ?></p>
                        </div>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                        <div class="b-callback__steps-info">
                            <h4><? $APPLICATION->IncludeFile('/local/include/steps/info-head-3.php'); ?></h4>
                            <p><? $APPLICATION->IncludeFile('/local/include/steps/info-text-3.php'); ?></p>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </section>
    

    <section class="b-map" id="map">
        <div class="container-fluid">
            <div class="row">
                <div class="b-contacts">
                    <? $APPLICATION->IncludeFile('/local/include/map/img.php'); ?>
                    <div class="b-contacts__info">
                        <? $APPLICATION->IncludeFile('/local/include/map/contacts.php'); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>

    
    <? $APPLICATION->IncludeFile('/local/include/modals/successForm.php'); ?>
<?
require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');
?>